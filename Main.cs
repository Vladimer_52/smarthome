﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Speech.Recognition;
using System.IO.Ports;

namespace SmartHome
{
    public partial class Main : Form
    {
        //флаги, включен ли свет
        bool fl_living;
        bool fl_storage;
        bool fl_bedroom;
        bool fl_kitchen;
        bool fl_ensuite;
        bool fl_hall;
        bool fl_bath1;
        bool fl_bath2;
        //====================//

        // //флаги, в фокусе ли объект
       bool fl_light_living = false;
       bool fl_slide_living = false;

       





        public Main()
        {
            TopMost = true;
            fl_living = true;
            fl_storage = true;
            fl_bedroom = true;
            fl_kitchen = true;
            fl_ensuite = true;
            fl_hall = true;
            fl_bath1 = true;
            fl_bath2 = true;


            InitializeComponent();

        }


        static Label l;
        void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result.Confidence > 0.7)
            {

               if (e.Result.Text == "включить свет в зале") Living();
               if (e.Result.Text == "включить свет в кладовке")Storage();
               if (e.Result.Text == "включить свет в кухне") Kitchen();
               if (e.Result.Text == "включить свет в туалете") Bath1();
               if (e.Result.Text == "включить свет в спальне") Bedroom();
               if (e.Result.Text == "включить свет в прихожей") Hall();
               if (e.Result.Text == "включить свет в ванной") Bath2();
               if (e.Result.Text == "включить свет в кабинете") Ensuite();

               if (e.Result.Text == "выключить свет в зале") Living();
               if (e.Result.Text == "выключить свет в кладовке") Storage();
               if (e.Result.Text == "выключить свет в кухне") Kitchen();
               if (e.Result.Text == "выключить свет в туалете") Bath1();
               if (e.Result.Text == "выключить свет в спальне") Bedroom();
               if (e.Result.Text == "выключить свет в прихожей") Hall();
               if (e.Result.Text == "выключить свет в ванной") Bath2();
               if (e.Result.Text == "выключить свет в кабинете") Ensuite();

                if (e.Result.Text == "открыть гугл") System.Diagnostics.Process.Start("http://google.com");
                if (e.Result.Text == "открыть яндекс") System.Diagnostics.Process.Start("http://ya.ru");
                if (e.Result.Text == "открыть вконтакте") System.Diagnostics.Process.Start(" https://vk.com/");
           

               l.Text = e.Result.Text;
            }
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Minimaze(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void b1_Click(object sender, EventArgs e)// кнопка температура
        {
            b1.Normalcolor = Color.FromArgb(222, 99, 38);
            b2.Normalcolor = Color.FromArgb(104, 104, 106);
        }

        private void b2_Click(object sender, EventArgs e)// кнопка свет
        {
            b2.Normalcolor = Color.FromArgb(222, 99, 38);
            b1.Normalcolor = Color.FromArgb(104, 104, 106);
     
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            serialPort.Open();
            serialPort.ReadTimeout = 2000;

            l = label9;
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ru-ru");
            SpeechRecognitionEngine sre = new SpeechRecognitionEngine(ci);
            sre.SetInputToDefaultAudioDevice();

            sre.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechRecognized);


            Choices numbers = new Choices();
            numbers.Add(new string[] {
                
                //сайты
                                        "открыть гугл", "открыть вконтакте", "открыть яндекс",

                //свет
                                       "включить свет в зале", 
                                       "включить свет в кладовке", 
                                       "включить свет в кухне", 
                                       "включить свет в туалете", 
                                       "включить свет в спальне", 
                                       "включить свет в прихожей", 
                                       "включить свет в ванной", 
                                       "включить свет в кабинете", 

                                       "выключить свет в зале", 
                                       "выключить свет в кладовке", 
                                       "выключить свет в кухне", 
                                       "выключить свет в туалете", 
                                       "выключить свет в спальне", 
                                       "выключить свет в прихожей", 
                                       "выключить свет в ванной", 
                                       "выключить свет в кабинете", 
            });


            GrammarBuilder gb = new GrammarBuilder();
            gb.Culture = ci;
            gb.Append(numbers);


            Grammar g = new Grammar(gb);
            sre.LoadGrammar(g);

            sre.RecognizeAsync(RecognizeMode.Multiple);
            
            
            //===делаем кнопки круглыми===//
            #region Circle
            System.Drawing.Drawing2D.GraphicsPath Button_Path1 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path1.AddEllipse(0, 0, this.light_storage.Width, this.light_storage.Height);
            Region Button_Region1 = new Region(Button_Path1);
            this.light_storage.Region = Button_Region1;

            System.Drawing.Drawing2D.GraphicsPath Button_Path2 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path2.AddEllipse(0, 0, this.light_living.Width, this.light_living.Height);
            Region Button_Region2 = new Region(Button_Path2);
            this.light_living.Region = Button_Region2;

            System.Drawing.Drawing2D.GraphicsPath Button_Path3 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path3.AddEllipse(0, 0, this.light_bedroom.Width, this.light_bedroom.Height);
            Region Button_Region3 = new Region(Button_Path3);
            this.light_bedroom.Region = Button_Region3;

            System.Drawing.Drawing2D.GraphicsPath Button_Path4 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path4.AddEllipse(0, 0, this.light_kitchen.Width, this.light_kitchen.Height);
            Region Button_Region4 = new Region(Button_Path4);
            this.light_kitchen.Region = Button_Region4;

            System.Drawing.Drawing2D.GraphicsPath Button_Path5 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path5.AddEllipse(0, 0, this.light_ensuite.Width, this.light_ensuite.Height);
            Region Button_Region5 = new Region(Button_Path5);
            this.light_ensuite.Region = Button_Region5;

            System.Drawing.Drawing2D.GraphicsPath Button_Path6 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path6.AddEllipse(0, 0, this.light_hall.Width, this.light_hall.Height);
            Region Button_Region6 = new Region(Button_Path6);
            this.light_hall.Region = Button_Region6;

            System.Drawing.Drawing2D.GraphicsPath Button_Path7 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path7.AddEllipse(0, 0, this.light_bath1.Width, this.light_bath1.Height);
            Region Button_Region7 = new Region(Button_Path7);
            this.light_bath1.Region = Button_Region7;

            System.Drawing.Drawing2D.GraphicsPath Button_Path8 = new System.Drawing.Drawing2D.GraphicsPath();
            Button_Path8.AddEllipse(0, 0, this.light_bath2.Width, this.light_bath2.Height);
            Region Button_Region8 = new Region(Button_Path8);
            this.light_bath2.Region = Button_Region8;



            System.Drawing.Drawing2D.GraphicsPath Button_progress = new System.Drawing.Drawing2D.GraphicsPath();
            Button_progress.AddEllipse(0, 0, this.light_living.Width, this.light_living.Height);
            Region Button_progress_r = new Region(Button_progress);
            this.progress.Region = Button_progress_r;

            #endregion
            //============================//


            //скрываем все ненужные элементы
            light_living.Visible = false;
            slide_living.Visible = false;
            text_living.Visible = false;


        }


         bool keyDown = false;
         int x = 0, y = 0;
 
        
 
    
    
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            keyDown = true;
            x = e.X;
            y = e.Y;
        }

        private void Panel_Temp_MouseDown(object sender, MouseEventArgs e)
        {
           keyDown = true;
            x = e.X;
            y = e.Y;
        }
        

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            keyDown = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!keyDown) return;
            this.Left += e.X - x;
            this.Top += e.Y - y;
        }

        private void Panel_Temp_MouseUp(object sender, MouseEventArgs e)
        {
            keyDown = false;
        }

        private void Panel_Temp_MouseMove(object sender, MouseEventArgs e)
        {
            if (!keyDown) return;
            this.Left += e.X - x;
            this.Top += e.Y - y;
        }

        private void Properties_buttonClick(object sender, EventArgs e)
        {
          /*  Properties_My F2 = new Properties_My(this);
            F2.ShowDialog();*/
            anim_settings.ShowSync(settings_panel);

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuTileButton1_Load(object sender, EventArgs e)
        {

        }

        //===Обработка событий кнопок===//
        #region Lights_button
        private void living_Click_1(object sender, EventArgs e)
        {
            Living();
        }

        private void light_storage_Click(object sender, EventArgs e)
        {
            Storage();
        }

        private void light_kitchen_Click(object sender, EventArgs e)
        {
            Kitchen();
        }

        private void light_bath1_Click(object sender, EventArgs e)
        {
            Bath1();
        }

        private void light_bedroom_Click(object sender, EventArgs e)
        {
            Bedroom();
        }

        private void light_hall_Click(object sender, EventArgs e)
        {
            Hall();
        }

        private void light_bath2_Click(object sender, EventArgs e)
        {
            Bath2();
        }

        private void light_ensuite_Click(object sender, EventArgs e)
        {
            Ensuite();
        }
        #endregion
        //====================================================//
        //===Обработка событий включения и выключения света===//
        #region Lights
        void Living()
        {
            if (fl_living)
            {
                serialPort.Write("light living on");
                if (serialPort.ReadExisting() == "1")
                {
                    Log.AppendText("Свет в зале включен" + "\n");
                    light_living.BackColor = Color.Yellow;
                    fl_living = !fl_living;
                }
            }
            else
            {
                serialPort.Write("light living on");
                if (serialPort.ReadExisting() == "0")
                {
                    light_living.BackColor = Color.Silver;
                    fl_living = !fl_living;
                    Log.AppendText("Свет в зале выключен" + "\n");
                }
            }
        }

        void Storage()
        {
            if (fl_storage)
            {
                serialPort.Write("light storage on");
                if (serialPort.ReadExisting() == "1")
                {
                    light_storage.BackColor = Color.Yellow;
                    fl_storage = !fl_storage;
                    Log.AppendText("Свет в кладовке включен" + "\n");
                }
            }
            else
            {
                serialPort.Write("light storage on");
                if (serialPort.ReadExisting() == "0")
                {
                    light_storage.BackColor = Color.Silver;
                    fl_storage = !fl_storage;
                    Log.AppendText("Свет в кладовке выключен" + "\n");
                }
            }
        }

        void Kitchen()
        {
            if (fl_kitchen)
            {
                serialPort.Write("light kitchen on");
                if (serialPort.ReadExisting() == "1")
                {
                    light_kitchen.BackColor = Color.Yellow;
                    fl_kitchen = !fl_kitchen;
                    Log.AppendText("Свет на кухне включен" + "\n");
                }
            }
            else
            {
                serialPort.Write("light kitchen on");
                if (serialPort.ReadExisting() == "0")
                {

                    light_kitchen.BackColor = Color.Silver;
                    fl_kitchen = !fl_kitchen;
                    Log.AppendText("Свет на кухне выключен" + "\n");
                }
            }
        }

        void Bath1()
        {
            if (fl_bath1)
            {
                serialPort.Write("light bath on");
                if (serialPort.ReadExisting() == "1")
                {
                    light_bath1.BackColor = Color.Yellow;
                    fl_bath1 = !fl_bath1;
                    Log.AppendText("Свет в туалете включен" + "\n");
                }
            }
            else
            {
                serialPort.Write("light bath on");
                if (serialPort.ReadExisting() == "0")
                {
                    light_bath1.BackColor = Color.Silver;
                    fl_bath1 = !fl_bath1;
                    Log.AppendText("Свет в туалете выключен" + "\n");
                }
            }
        }

        void Bedroom()
        {
            if (fl_bedroom)
            {
                serialPort.Write("light bedroom on");
                if (serialPort.ReadExisting() == "1")
                {
                    light_bedroom.BackColor = Color.Yellow;
                    fl_bedroom = !fl_bedroom;
                    Log.AppendText("Свет в спальне включен" + "\n");
                }
            }
            else
            {
                serialPort.Write("light bedroom on");
                if (serialPort.ReadExisting() == "0")
                {
                    light_bedroom.BackColor = Color.Silver;
                    fl_bedroom = !fl_bedroom;
                    Log.AppendText("Свет в спальне выключен" + "\n");
                }
            }
        }

        void Hall()
        {
            if (fl_hall)
            {
                serialPort.Write("light hall on");
                if (serialPort.ReadExisting() == "1")
                {
                    light_hall.BackColor = Color.Yellow;
                    fl_hall = !fl_hall;
                    Log.AppendText("Свет в прихожей включен" + "\n");
                }

            }
            else
            {
                serialPort.Write("light hall on");
                if (serialPort.ReadExisting() == "0")
                {
                    light_hall.BackColor = Color.Silver;
                    fl_hall = !fl_hall;
                    Log.AppendText("Свет в прихожей выключен" + "\n");
                }
            }
        }

        void Bath2()
        {
            if (fl_bath2)
            {
                serialPort.Write("light bath2 on");
                if (serialPort.ReadExisting() == "1")
                {
                    light_bath2.BackColor = Color.Yellow;
                    fl_bath2 = !fl_bath2;
                    Log.AppendText("Свет в ванной включен" + "\n");
                }
            }
            else
            {
                serialPort.Write("light bath2 on");
                if (serialPort.ReadExisting() == "0")
                {
                    light_bath2.BackColor = Color.Silver;
                    fl_bath2 = !fl_bath2;
                    Log.AppendText("Свет в ванной выключен" + "\n");
                }
            }
        }

        void Ensuite()
        {
            if (fl_ensuite)
            {
                serialPort.Write("light ensuite on");
                if (serialPort.ReadExisting() == "1")
                {
                    light_ensuite.BackColor = Color.Yellow;
                    fl_ensuite = !fl_ensuite;
                    Log.AppendText("Свет в кабинете включен" + "\n");
                }
            }
            else
            {
                serialPort.Write("light ensuite on");
                if (serialPort.ReadExisting() == "0")
                {
                    light_ensuite.BackColor = Color.Silver;
                    fl_ensuite = !fl_ensuite;
                    Log.AppendText("Свет в кабинете выключен" + "\n");
                }
            }
        }
        #endregion

        //====================================================//
        private void com_rimer_Tick(object sender, EventArgs e)
        {
            if ((Command.Text == ">Время") || (Command.Text == ">время") || (Command.Text == ">time")) { Log.AppendText("время: " + Convert.ToString(DateTime.Now) + "\n"); Command.Text = ""; }
            if ((Command.Text == ">очистить") || (Command.Text == ">Очистить") || (Command.Text == ">clear")) { Log.Clear(); Command.Text = ""; }
            if (DateTime.Now.Second == 0 && DateTime.Now.Minute == 00) { Log.AppendText("время: " + Convert.ToString(DateTime.Now.Hour) + ":" + Convert.ToString(DateTime.Now.Minute) + "\n"); }
            if(Command.Text == ">help"){ Log.AppendText("доступные команды: \n время/time \n очистить/clear \n " +
                                                        "госовые команды: открыть гугл \n открыть вконтакте \n открыть яндекc \n" + 
                                                        "включить/выключить свет в\n зале\n кладовке\n кухне\n туалете\n спальне\n прихожей\n ванной\n кабинете\n"); Command.Text = "";
            }

            //обработка событий, связаных с закрытием слайдеров

            if (fl_light_living && fl_slide_living) { anim_delay.Enabled = true; }

            //=================================================//
        }

        
        private void Command_TextChanged(object sender, EventArgs e)
        {
            if (!Command.Text.StartsWith(">"))
            {
                Command.Text = string.Concat(">", Command.Text);
                Command.Select(Command.Text.Length, 0);
            }
        }

        private void progress_MouseHover(object sender, EventArgs e)
        {
            
           
        }
        int timer = 0;

        private void anim_delay_Tick(object sender, EventArgs e)
        {
            timer++;
            if (timer == 10) {
         
                anim_delay.Enabled = false;
            }

        }
        private void progress_MouseEnter(object sender, EventArgs e)
        {
            progress.BorderStyle = BorderStyle.FixedSingle; 
            bunifuTransition1.ShowSync(light_living);
            bunifuTransition1.ShowSync(slide_living);
      
        }

        private void slide_living_ValueChanged(object sender, EventArgs e)
        {
            text_living.Visible = true;
            text_living.Text = Convert.ToString(slide_living.Value);
           
        }

        private void light_living_MouseLeave(object sender, EventArgs e)
        {
    
        }

        private void slide_living_MouseLeave(object sender, EventArgs e)
        {
        
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void progress_Click(object sender, EventArgs e)
        {
            bunifuTransition1.HideSync(light_living);
            bunifuTransition1.HideSync(slide_living);
            text_living.Visible = false;
            Log.AppendText("температура в зале: " + text_living.Text + "°С\n");

        }

        private void progress_MouseLeave(object sender, EventArgs e)
        {
            progress.BorderStyle = BorderStyle.None; 
        }

        private void button_apply_Click(object sender, EventArgs e)
        {
            if (check_auto.Checked)  Log.AppendText("Автоматическое освещение включено\n"); else Log.AppendText("Автоматическое освещение выключено\n");
            anim_settings.HideSync(settings_panel);
        }

        private void ComboSerial_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            ComboSerial.Items.Clear();
            ComboSerial.Items.AddRange(ports);
            serialPort.PortName = ComboSerial.Text;
        }

       

  
    }
}

﻿namespace SmartHome
{
    partial class Main
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation4 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation3 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.settings_panel = new System.Windows.Forms.Panel();
            this.button_apply = new Bunifu.Framework.UI.BunifuImageButton();
            this.check_auto = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.b1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.b2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.Command = new System.Windows.Forms.TextBox();
            this.Log = new System.Windows.Forms.RichTextBox();
            this.light_living = new Bunifu.Framework.UI.BunifuImageButton();
            this.light_storage = new Bunifu.Framework.UI.BunifuImageButton();
            this.light_bedroom = new Bunifu.Framework.UI.BunifuImageButton();
            this.light_ensuite = new Bunifu.Framework.UI.BunifuImageButton();
            this.light_bath2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.light_bath1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.light_hall = new Bunifu.Framework.UI.BunifuImageButton();
            this.light_kitchen = new Bunifu.Framework.UI.BunifuImageButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.com_rimer = new System.Windows.Forms.Timer(this.components);
            this.progress = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.bunifuTransition1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.slide_living = new Bunifu.Framework.UI.BunifuSlider();
            this.text_living = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.anim_delay = new System.Windows.Forms.Timer(this.components);
            this.anim_settings = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.ComboSerial = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            this.panel2.SuspendLayout();
            this.settings_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.button_apply)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.light_living)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_storage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_bedroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_ensuite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_bath2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_bath1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_hall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_kitchen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 0;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(112)))), ((int)(((byte)(186)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.bunifuImageButton1);
            this.panel1.Controls.Add(this.bunifuImageButton3);
            this.panel1.Controls.Add(this.bunifuImageButton2);
            this.anim_settings.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1349, 62);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Black;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.anim_settings.SetDecoration(this.panel7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel7, BunifuAnimatorNS.DecorationType.None);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(1345, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(2, 60);
            this.panel7.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(305, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 18);
            this.label9.TabIndex = 1;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.anim_settings.SetDecoration(this.bunifuImageButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.bunifuImageButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.ImageActive")));
            this.bunifuImageButton1.InitialImage = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(1306, 11);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(30, 30);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 0;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.WaitOnLoad = true;
            this.bunifuImageButton1.Zoom = 30;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.anim_settings.SetDecoration(this.bunifuImageButton3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.bunifuImageButton3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuImageButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.Image")));
            this.bunifuImageButton3.ImageActive = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.ImageActive")));
            this.bunifuImageButton3.InitialImage = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(13, 13);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(30, 30);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton3.TabIndex = 0;
            this.bunifuImageButton3.TabStop = false;
            this.bunifuImageButton3.WaitOnLoad = true;
            this.bunifuImageButton3.Zoom = 30;
            this.bunifuImageButton3.Click += new System.EventHandler(this.Properties_buttonClick);
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.anim_settings.SetDecoration(this.bunifuImageButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.bunifuImageButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.ImageActive")));
            this.bunifuImageButton2.InitialImage = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(1261, 11);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(30, 30);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 0;
            this.bunifuImageButton2.TabStop = false;
            this.bunifuImageButton2.WaitOnLoad = true;
            this.bunifuImageButton2.Zoom = 30;
            this.bunifuImageButton2.Click += new System.EventHandler(this.Minimaze);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(41)))));
            this.panel2.Controls.Add(this.settings_panel);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.b1);
            this.panel2.Controls.Add(this.b2);
            this.anim_settings.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 62);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 499);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // settings_panel
            // 
            this.settings_panel.Controls.Add(this.label2);
            this.settings_panel.Controls.Add(this.ComboSerial);
            this.settings_panel.Controls.Add(this.button_apply);
            this.settings_panel.Controls.Add(this.check_auto);
            this.anim_settings.SetDecoration(this.settings_panel, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.settings_panel, BunifuAnimatorNS.DecorationType.None);
            this.settings_panel.Location = new System.Drawing.Point(0, 185);
            this.settings_panel.Name = "settings_panel";
            this.settings_panel.Size = new System.Drawing.Size(200, 193);
            this.settings_panel.TabIndex = 38;
            this.settings_panel.Visible = false;
            // 
            // button_apply
            // 
            this.button_apply.BackColor = System.Drawing.Color.Transparent;
            this.button_apply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.anim_settings.SetDecoration(this.button_apply, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.button_apply, BunifuAnimatorNS.DecorationType.None);
            this.button_apply.Image = ((System.Drawing.Image)(resources.GetObject("button_apply.Image")));
            this.button_apply.ImageActive = ((System.Drawing.Image)(resources.GetObject("button_apply.ImageActive")));
            this.button_apply.InitialImage = null;
            this.button_apply.Location = new System.Drawing.Point(64, 149);
            this.button_apply.Name = "button_apply";
            this.button_apply.Size = new System.Drawing.Size(60, 30);
            this.button_apply.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.button_apply.TabIndex = 39;
            this.button_apply.TabStop = false;
            this.button_apply.WaitOnLoad = true;
            this.button_apply.Zoom = 30;
            this.button_apply.Click += new System.EventHandler(this.button_apply_Click);
            // 
            // check_auto
            // 
            this.check_auto.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.check_auto, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this.check_auto, BunifuAnimatorNS.DecorationType.None);
            this.check_auto.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.check_auto.ForeColor = System.Drawing.Color.White;
            this.check_auto.Location = new System.Drawing.Point(4, 32);
            this.check_auto.Name = "check_auto";
            this.check_auto.Size = new System.Drawing.Size(190, 19);
            this.check_auto.TabIndex = 38;
            this.check_auto.Text = "автоматическое освещение";
            this.check_auto.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Black;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.anim_settings.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(2, 497);
            this.panel6.TabIndex = 37;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.anim_settings.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 497);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 2);
            this.panel4.TabIndex = 36;
            // 
            // b1
            // 
            this.b1.Activecolor = System.Drawing.Color.Yellow;
            this.b1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(106)))));
            this.b1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.b1.BorderRadius = 0;
            this.b1.ButtonText = "    Температура";
            this.b1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_settings.SetDecoration(this.b1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.b1, BunifuAnimatorNS.DecorationType.None);
            this.b1.DisabledColor = System.Drawing.Color.Gray;
            this.b1.Iconcolor = System.Drawing.Color.Transparent;
            this.b1.Iconimage = ((System.Drawing.Image)(resources.GetObject("b1.Iconimage")));
            this.b1.Iconimage_right = null;
            this.b1.Iconimage_right_Selected = null;
            this.b1.Iconimage_Selected = null;
            this.b1.IconMarginLeft = 0;
            this.b1.IconMarginRight = 0;
            this.b1.IconRightVisible = true;
            this.b1.IconRightZoom = 0D;
            this.b1.IconVisible = true;
            this.b1.IconZoom = 40D;
            this.b1.IsTab = false;
            this.b1.Location = new System.Drawing.Point(0, 36);
            this.b1.Name = "b1";
            this.b1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(106)))));
            this.b1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(99)))), ((int)(((byte)(40)))));
            this.b1.OnHoverTextColor = System.Drawing.Color.White;
            this.b1.selected = false;
            this.b1.Size = new System.Drawing.Size(200, 48);
            this.b1.TabIndex = 0;
            this.b1.Text = "    Температура";
            this.b1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b1.Textcolor = System.Drawing.Color.White;
            this.b1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // b2
            // 
            this.b2.Activecolor = System.Drawing.Color.Yellow;
            this.b2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(106)))));
            this.b2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.b2.BorderRadius = 0;
            this.b2.ButtonText = "           свет";
            this.b2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.anim_settings.SetDecoration(this.b2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.b2, BunifuAnimatorNS.DecorationType.None);
            this.b2.DisabledColor = System.Drawing.Color.Gray;
            this.b2.Iconcolor = System.Drawing.Color.Transparent;
            this.b2.Iconimage = ((System.Drawing.Image)(resources.GetObject("b2.Iconimage")));
            this.b2.Iconimage_right = null;
            this.b2.Iconimage_right_Selected = null;
            this.b2.Iconimage_Selected = null;
            this.b2.IconMarginLeft = 0;
            this.b2.IconMarginRight = 0;
            this.b2.IconRightVisible = true;
            this.b2.IconRightZoom = 0D;
            this.b2.IconVisible = true;
            this.b2.IconZoom = 40D;
            this.b2.IsTab = false;
            this.b2.Location = new System.Drawing.Point(0, 88);
            this.b2.Name = "b2";
            this.b2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(106)))));
            this.b2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(99)))), ((int)(((byte)(40)))));
            this.b2.OnHoverTextColor = System.Drawing.Color.White;
            this.b2.selected = false;
            this.b2.Size = new System.Drawing.Size(200, 48);
            this.b2.TabIndex = 0;
            this.b2.Text = "           свет";
            this.b2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.b2.Textcolor = System.Drawing.Color.White;
            this.b2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.anim_settings.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(200, 559);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1149, 2);
            this.panel3.TabIndex = 35;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.anim_settings.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(1347, 62);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(2, 497);
            this.panel5.TabIndex = 36;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.Command);
            this.panel8.Controls.Add(this.Log);
            this.anim_settings.SetDecoration(this.panel8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.panel8, BunifuAnimatorNS.DecorationType.None);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(1147, 62);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(200, 497);
            this.panel8.TabIndex = 37;
            // 
            // Command
            // 
            this.Command.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuTransition1.SetDecoration(this.Command, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this.Command, BunifuAnimatorNS.DecorationType.None);
            this.Command.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Command.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Command.Location = new System.Drawing.Point(0, 470);
            this.Command.Name = "Command";
            this.Command.Size = new System.Drawing.Size(200, 27);
            this.Command.TabIndex = 0;
            this.Command.Text = ">";
            this.Command.TextChanged += new System.EventHandler(this.Command_TextChanged);
            // 
            // Log
            // 
            this.anim_settings.SetDecoration(this.Log, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.Log, BunifuAnimatorNS.DecorationType.None);
            this.Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Log.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.Log.Location = new System.Drawing.Point(0, 0);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(200, 497);
            this.Log.TabIndex = 0;
            this.Log.Text = "";
            // 
            // light_living
            // 
            this.light_living.BackColor = System.Drawing.Color.Silver;
            this.light_living.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_living, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_living, BunifuAnimatorNS.DecorationType.None);
            this.light_living.Image = ((System.Drawing.Image)(resources.GetObject("light_living.Image")));
            this.light_living.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_living.ImageActive")));
            this.light_living.InitialImage = null;
            this.light_living.Location = new System.Drawing.Point(354, 305);
            this.light_living.Name = "light_living";
            this.light_living.Size = new System.Drawing.Size(30, 30);
            this.light_living.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_living.TabIndex = 39;
            this.light_living.TabStop = false;
            this.light_living.WaitOnLoad = true;
            this.light_living.Zoom = 5;
            this.light_living.Click += new System.EventHandler(this.living_Click_1);
            this.light_living.MouseLeave += new System.EventHandler(this.light_living_MouseLeave);
            // 
            // light_storage
            // 
            this.light_storage.BackColor = System.Drawing.Color.Silver;
            this.light_storage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_storage, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_storage, BunifuAnimatorNS.DecorationType.None);
            this.light_storage.Image = ((System.Drawing.Image)(resources.GetObject("light_storage.Image")));
            this.light_storage.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_storage.ImageActive")));
            this.light_storage.InitialImage = null;
            this.light_storage.Location = new System.Drawing.Point(342, 382);
            this.light_storage.Name = "light_storage";
            this.light_storage.Size = new System.Drawing.Size(30, 30);
            this.light_storage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_storage.TabIndex = 40;
            this.light_storage.TabStop = false;
            this.light_storage.WaitOnLoad = true;
            this.light_storage.Zoom = 5;
            this.light_storage.Click += new System.EventHandler(this.light_storage_Click);
            // 
            // light_bedroom
            // 
            this.light_bedroom.BackColor = System.Drawing.Color.Silver;
            this.light_bedroom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_bedroom, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_bedroom, BunifuAnimatorNS.DecorationType.None);
            this.light_bedroom.Image = ((System.Drawing.Image)(resources.GetObject("light_bedroom.Image")));
            this.light_bedroom.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_bedroom.ImageActive")));
            this.light_bedroom.InitialImage = null;
            this.light_bedroom.Location = new System.Drawing.Point(803, 183);
            this.light_bedroom.Name = "light_bedroom";
            this.light_bedroom.Size = new System.Drawing.Size(30, 30);
            this.light_bedroom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_bedroom.TabIndex = 41;
            this.light_bedroom.TabStop = false;
            this.light_bedroom.WaitOnLoad = true;
            this.light_bedroom.Zoom = 5;
            this.light_bedroom.Click += new System.EventHandler(this.light_bedroom_Click);
            // 
            // light_ensuite
            // 
            this.light_ensuite.BackColor = System.Drawing.Color.Silver;
            this.light_ensuite.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_ensuite, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_ensuite, BunifuAnimatorNS.DecorationType.None);
            this.light_ensuite.Image = ((System.Drawing.Image)(resources.GetObject("light_ensuite.Image")));
            this.light_ensuite.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_ensuite.ImageActive")));
            this.light_ensuite.InitialImage = null;
            this.light_ensuite.Location = new System.Drawing.Point(1004, 168);
            this.light_ensuite.Name = "light_ensuite";
            this.light_ensuite.Size = new System.Drawing.Size(30, 30);
            this.light_ensuite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_ensuite.TabIndex = 42;
            this.light_ensuite.TabStop = false;
            this.light_ensuite.WaitOnLoad = true;
            this.light_ensuite.Zoom = 5;
            this.light_ensuite.Click += new System.EventHandler(this.light_ensuite_Click);
            // 
            // light_bath2
            // 
            this.light_bath2.BackColor = System.Drawing.Color.Silver;
            this.light_bath2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_bath2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_bath2, BunifuAnimatorNS.DecorationType.None);
            this.light_bath2.Image = ((System.Drawing.Image)(resources.GetObject("light_bath2.Image")));
            this.light_bath2.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_bath2.ImageActive")));
            this.light_bath2.InitialImage = null;
            this.light_bath2.Location = new System.Drawing.Point(1045, 317);
            this.light_bath2.Name = "light_bath2";
            this.light_bath2.Size = new System.Drawing.Size(30, 30);
            this.light_bath2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_bath2.TabIndex = 43;
            this.light_bath2.TabStop = false;
            this.light_bath2.WaitOnLoad = true;
            this.light_bath2.Zoom = 5;
            this.light_bath2.Click += new System.EventHandler(this.light_bath2_Click);
            // 
            // light_bath1
            // 
            this.light_bath1.BackColor = System.Drawing.Color.Silver;
            this.light_bath1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_bath1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_bath1, BunifuAnimatorNS.DecorationType.None);
            this.light_bath1.Image = ((System.Drawing.Image)(resources.GetObject("light_bath1.Image")));
            this.light_bath1.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_bath1.ImageActive")));
            this.light_bath1.InitialImage = null;
            this.light_bath1.Location = new System.Drawing.Point(836, 382);
            this.light_bath1.Name = "light_bath1";
            this.light_bath1.Size = new System.Drawing.Size(30, 30);
            this.light_bath1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_bath1.TabIndex = 44;
            this.light_bath1.TabStop = false;
            this.light_bath1.WaitOnLoad = true;
            this.light_bath1.Zoom = 5;
            this.light_bath1.Click += new System.EventHandler(this.light_bath1_Click);
            // 
            // light_hall
            // 
            this.light_hall.BackColor = System.Drawing.Color.Silver;
            this.light_hall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_hall, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_hall, BunifuAnimatorNS.DecorationType.None);
            this.light_hall.Image = ((System.Drawing.Image)(resources.GetObject("light_hall.Image")));
            this.light_hall.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_hall.ImageActive")));
            this.light_hall.InitialImage = null;
            this.light_hall.Location = new System.Drawing.Point(950, 468);
            this.light_hall.Name = "light_hall";
            this.light_hall.Size = new System.Drawing.Size(30, 30);
            this.light_hall.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_hall.TabIndex = 45;
            this.light_hall.TabStop = false;
            this.light_hall.WaitOnLoad = true;
            this.light_hall.Zoom = 5;
            this.light_hall.Click += new System.EventHandler(this.light_hall_Click);
            // 
            // light_kitchen
            // 
            this.light_kitchen.BackColor = System.Drawing.Color.Silver;
            this.light_kitchen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.anim_settings.SetDecoration(this.light_kitchen, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.light_kitchen, BunifuAnimatorNS.DecorationType.None);
            this.light_kitchen.Image = ((System.Drawing.Image)(resources.GetObject("light_kitchen.Image")));
            this.light_kitchen.ImageActive = ((System.Drawing.Image)(resources.GetObject("light_kitchen.ImageActive")));
            this.light_kitchen.InitialImage = null;
            this.light_kitchen.Location = new System.Drawing.Point(708, 396);
            this.light_kitchen.Name = "light_kitchen";
            this.light_kitchen.Size = new System.Drawing.Size(30, 30);
            this.light_kitchen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.light_kitchen.TabIndex = 46;
            this.light_kitchen.TabStop = false;
            this.light_kitchen.WaitOnLoad = true;
            this.light_kitchen.Zoom = 5;
            this.light_kitchen.Click += new System.EventHandler(this.light_kitchen_Click);
            // 
            // pictureBox1
            // 
            this.bunifuTransition1.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(200, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1002, 497);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // com_rimer
            // 
            this.com_rimer.Enabled = true;
            this.com_rimer.Interval = 700;
            this.com_rimer.Tick += new System.EventHandler(this.com_rimer_Tick);
            // 
            // progress
            // 
            this.progress.animated = true;
            this.progress.animationIterval = 1;
            this.progress.animationSpeed = 3;
            this.progress.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.progress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(164)))), ((int)(((byte)(88)))));
            this.progress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("progress.BackgroundImage")));
            this.progress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.anim_settings.SetDecoration(this.progress, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.progress, BunifuAnimatorNS.DecorationType.None);
            this.progress.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.progress.ForeColor = System.Drawing.Color.SlateBlue;
            this.progress.LabelVisible = false;
            this.progress.LineProgressThickness = 8;
            this.progress.LineThickness = 5;
            this.progress.Location = new System.Drawing.Point(354, 272);
            this.progress.Margin = new System.Windows.Forms.Padding(0);
            this.progress.MaxValue = 100;
            this.progress.Name = "progress";
            this.progress.ProgressBackColor = System.Drawing.Color.Silver;
            this.progress.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(112)))), ((int)(((byte)(186)))));
            this.progress.Size = new System.Drawing.Size(30, 30);
            this.progress.TabIndex = 47;
            this.progress.Value = 50;
            this.progress.Click += new System.EventHandler(this.progress_Click);
            this.progress.MouseEnter += new System.EventHandler(this.progress_MouseEnter);
            this.progress.MouseLeave += new System.EventHandler(this.progress_MouseLeave);
            this.progress.MouseHover += new System.EventHandler(this.progress_MouseHover);
            // 
            // bunifuTransition1
            // 
            this.bunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.Scale;
            this.bunifuTransition1.Cursor = null;
            animation4.AnimateOnlyDifferences = true;
            animation4.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.BlindCoeff")));
            animation4.LeafCoeff = 0F;
            animation4.MaxTime = 1F;
            animation4.MinTime = 0F;
            animation4.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.MosaicCoeff")));
            animation4.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation4.MosaicShift")));
            animation4.MosaicSize = 0;
            animation4.Padding = new System.Windows.Forms.Padding(0);
            animation4.RotateCoeff = 0F;
            animation4.RotateLimit = 0F;
            animation4.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.ScaleCoeff")));
            animation4.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.SlideCoeff")));
            animation4.TimeCoeff = 0F;
            animation4.TransparencyCoeff = 0F;
            this.bunifuTransition1.DefaultAnimation = animation4;
            // 
            // slide_living
            // 
            this.slide_living.BackColor = System.Drawing.Color.Transparent;
            this.slide_living.BackgroudColor = System.Drawing.Color.DarkGray;
            this.slide_living.BorderRadius = 10;
            this.anim_settings.SetDecoration(this.slide_living, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.slide_living, BunifuAnimatorNS.DecorationType.None);
            this.slide_living.IndicatorColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(112)))), ((int)(((byte)(186)))));
            this.slide_living.Location = new System.Drawing.Point(390, 305);
            this.slide_living.MaximumValue = 100;
            this.slide_living.Name = "slide_living";
            this.slide_living.Size = new System.Drawing.Size(108, 30);
            this.slide_living.TabIndex = 48;
            this.slide_living.Value = 0;
            this.slide_living.ValueChanged += new System.EventHandler(this.slide_living_ValueChanged);
            this.slide_living.MouseLeave += new System.EventHandler(this.slide_living_MouseLeave);
            // 
            // text_living
            // 
            this.text_living.BorderColorFocused = System.Drawing.Color.Blue;
            this.text_living.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.text_living.BorderColorMouseHover = System.Drawing.Color.Blue;
            this.text_living.BorderThickness = 3;
            this.text_living.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.bunifuTransition1.SetDecoration(this.text_living, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this.text_living, BunifuAnimatorNS.DecorationType.None);
            this.text_living.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.text_living.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.text_living.isPassword = false;
            this.text_living.Location = new System.Drawing.Point(406, 272);
            this.text_living.Margin = new System.Windows.Forms.Padding(4);
            this.text_living.Name = "text_living";
            this.text_living.Size = new System.Drawing.Size(71, 26);
            this.text_living.TabIndex = 49;
            this.text_living.Text = "25";
            this.text_living.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // anim_delay
            // 
            this.anim_delay.Tick += new System.EventHandler(this.anim_delay_Tick);
            // 
            // anim_settings
            // 
            this.anim_settings.AnimationType = BunifuAnimatorNS.AnimationType.Particles;
            this.anim_settings.Cursor = null;
            animation3.AnimateOnlyDifferences = true;
            animation3.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.BlindCoeff")));
            animation3.LeafCoeff = 0F;
            animation3.MaxTime = 1F;
            animation3.MinTime = 0F;
            animation3.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicCoeff")));
            animation3.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicShift")));
            animation3.MosaicSize = 1;
            animation3.Padding = new System.Windows.Forms.Padding(100, 50, 100, 150);
            animation3.RotateCoeff = 0F;
            animation3.RotateLimit = 0F;
            animation3.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.ScaleCoeff")));
            animation3.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.SlideCoeff")));
            animation3.TimeCoeff = 2F;
            animation3.TransparencyCoeff = 0F;
            this.anim_settings.DefaultAnimation = animation3;
            // 
            // ComboSerial
            // 
            this.anim_settings.SetDecoration(this.ComboSerial, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.ComboSerial, BunifuAnimatorNS.DecorationType.None);
            this.ComboSerial.FormattingEnabled = true;
            this.ComboSerial.Location = new System.Drawing.Point(8, 100);
            this.ComboSerial.Name = "ComboSerial";
            this.ComboSerial.Size = new System.Drawing.Size(165, 21);
            this.ComboSerial.TabIndex = 50;
            this.ComboSerial.SelectedIndexChanged += new System.EventHandler(this.ComboSerial_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Century Gothic", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(77, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 40);
            this.label1.TabIndex = 38;
            this.label1.Text = "SmartHome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.bunifuTransition1.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(8, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 16);
            this.label2.TabIndex = 51;
            this.label2.Text = "выберите порт подкл. уст-ва";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1349, 561);
            this.Controls.Add(this.text_living);
            this.Controls.Add(this.slide_living);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.light_kitchen);
            this.Controls.Add(this.light_hall);
            this.Controls.Add(this.light_bath1);
            this.Controls.Add(this.light_bath2);
            this.Controls.Add(this.light_ensuite);
            this.Controls.Add(this.light_bedroom);
            this.Controls.Add(this.light_storage);
            this.Controls.Add(this.light_living);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.bunifuTransition1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.anim_settings.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main";
            this.Text = "SmartHome";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.settings_panel.ResumeLayout(false);
            this.settings_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.button_apply)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.light_living)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_storage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_bedroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_ensuite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_bath2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_bath1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_hall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.light_kitchen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuFlatButton b1;
        private Bunifu.Framework.UI.BunifuFlatButton b2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private System.Windows.Forms.Label label9;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox Command;
        private System.Windows.Forms.RichTextBox Log;
        private Bunifu.Framework.UI.BunifuImageButton light_living;
        private Bunifu.Framework.UI.BunifuImageButton light_kitchen;
        private Bunifu.Framework.UI.BunifuImageButton light_hall;
        private Bunifu.Framework.UI.BunifuImageButton light_bath1;
        private Bunifu.Framework.UI.BunifuImageButton light_bath2;
        private Bunifu.Framework.UI.BunifuImageButton light_ensuite;
        private Bunifu.Framework.UI.BunifuImageButton light_bedroom;
        private Bunifu.Framework.UI.BunifuImageButton light_storage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer com_rimer;
        private Bunifu.Framework.UI.BunifuCircleProgressbar progress;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition1;
        private Bunifu.Framework.UI.BunifuMetroTextbox text_living;
        private Bunifu.Framework.UI.BunifuSlider slide_living;
        private System.Windows.Forms.Timer anim_delay;
        private BunifuAnimatorNS.BunifuTransition anim_settings;
        private System.Windows.Forms.Panel settings_panel;
        private System.Windows.Forms.CheckBox check_auto;
        private Bunifu.Framework.UI.BunifuImageButton button_apply;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.ComboBox ComboSerial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}


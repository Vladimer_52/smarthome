﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace SmartHome
{

    public partial class Properties_My : Form
    {
        Main F1;
        public Properties_My(Main F1_)
        {
            F1 = F1_;
            InitializeComponent();
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Properties_My_Load(object sender, EventArgs e)
        {

            string[] ports = SerialPort.GetPortNames();
            cbPorts.Items.Clear();
            cbPorts.Items.AddRange(ports);
        }
    }
}
